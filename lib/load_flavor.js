'use strict';

var path = require( 'path' );

module.exports = function( project ){
  var appRoot = project.root;
  try {
    var flavors = require(
      path.join(appRoot, 'config', 'flavors.js')
    );
    var length = ((flavors.flavors)||[]).length;
    var indexOfFlavor =  process.argv.indexOf('--flavor');
    if(indexOfFlavor < 0 || process.argv.length < indexOfFlavor + 1){
      project.ui.writeLine("No flavor given via '--flavor' argument.");
      return null;
    }
    var selectedFlavor = process.argv[ indexOfFlavor + 1 ].toUpperCase();
    for(var i = 0; i < length; i++){
      var flavor = flavors.flavors[i];
      if(flavor.name === selectedFlavor){
        project.ui.writeLine("Using flavor '" + flavor.name + "'")
        return flavor;
      }
    }
    project.ui.writeLine( "No flavor '" + selectedFlavor + "' found in 'config/flavors.js'.");
    return null;
  }
  catch( e ){
    project.ui.writeLine( "No 'config/flavors.js' file found. Create it!" );
    return null;
  }
};
