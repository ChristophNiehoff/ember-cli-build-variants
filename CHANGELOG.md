# Changelog

## master
* Updated build image to ember(-cli) 2.18.0

## 1.0.2
* Made slight changes to README.md docs.
* Update to ember(-cli) 2.18.0

## 1.0.1
* Added repository link to package.json.

## 1.0.0
* Initial release.
