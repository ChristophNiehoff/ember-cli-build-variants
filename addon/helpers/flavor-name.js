import { helper } from '@ember/component/helper';
import config from 'ember-get-config';

export function flavorName(/*params, hash*/) {
  return config.flavor.name;
}

export default helper(flavorName);
